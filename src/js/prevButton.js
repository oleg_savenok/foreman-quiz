export function prevButton(startTarget, finishTarget, foremanTarget) {
	if (!finishTarget.hasClass('is-hidden')) {
		finishTarget.addClass('is-hidden');
		foremanTarget.removeClass('is-hidden');
		startTarget.removeClass('is-hidden');
		$('#questionsSliderBG').removeClass('is-finish');
		startTarget.slick('slickPrev');
	} else {
		startTarget.slick('slickPrev');
	}
}