export function ajaxRequest(target, btn) {
	/*
	php
	$_POST["varname"]
	*/

	let data = decodeURI(target.serialize()).replace('%40', '@');

	console.log(data);

	$.ajax({
		url: target.attr('action'),
		method: 'POST',
		data: data,
		success: function(data) {
			btn.addClass('success');
		}
	})
};