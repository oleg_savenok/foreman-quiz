export function validRadioOptions(event, sliderAction, sliderContainer, changeBG = false, changeSendButtonText = false) {
	if (event.target.checked) {
		sliderAction.slick('slickNext');
	}

	if (changeBG) {
		setTimeout(function() {
			sliderContainer[0].style.backgroundImage = `url(./img/room-type/${event.target.defaultValue}.jpeg)`;
		}, 1.5*500);
	}

	if (changeSendButtonText) {
		if (event.target.id == "estimate") {
			console.log(event.target.id);
			$('#sendButton p')[0].innerText = 'Получить смету';
			$('#sendButton').addClass('estimate');
		}
	}
}