import {ajaxRequest} from './ajaxRequest';
import {videoAction} from './videoAction';
import {prevButton} from './prevButton';
import {inputAction} from './inputAction';
import {inputEnter} from './inputEnter';
import {validRadioOptions} from './validRadioOptions';

(function () {
	// SLICK INIT OPTIONS ----------------------------- //

	// Variables

	const questionSliderContainer = $('#questionSliderContainer');
	const	questionSlider = $('#questionSlider');
	const foremanSlider = $('#foremanSlider');
	const finishSlider = $('#finishSlider');
	const easeInOutQuart = 'cubic-bezier(0.77, 0, 0.175, 1)';
	const baseTD = 500;

	const videoTarget = $('#video');
	const videoSource = $('#video .video__source');

	const logo = $('#headerLogo');
	const startButton = $('#startButton');
	const contactInfo = $('.contactInfo');
	const contactButton = $('#contactButton');
	const shareButton = $('#shareButton');
	const prevButtonTarget = $('#prevButton');
	const pagesStatus = $('#pagesStatus');
	const discountStatus = $('#discountStatus');

	const planOptions = $('[name="plan"]');
	const serviceOptions = $('[name="service"]');
	const inputRoomType = $('#slideRoomType input');
	const labelRoomType = $('#slideRoomType label');

	const questionsSliderBG = $('#questionsSliderBG');

	const targetForm = $('#questionsForm, #finishForm');
	const sendButton = $('#sendButton');
	const buyButton = $('#buyButton');

	const inputNext = $('.question-slides__single__input span');

	const questionSliderSingle = ".question-slides__single";


	// Main slider

	const sliderCommonOptions = {
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		infinite: false,
		speed: 1.5*baseTD,
		cssEase: easeInOutQuart,
		draggable: false,
		accessibility: false,
		swipe: false,
		touchMove: false,
	};

	questionSlider.slick({
		asNavFor: foremanSlider,
		...sliderCommonOptions
	});

	finishSlider.slick({
		...sliderCommonOptions
	});

	// Image slider

	foremanSlider.slick({
		asNavFor: questionSlider,
		...sliderCommonOptions
	});

	// VIDEO ------------------------------------------ //

	// Play / Pause

	videoTarget.click(function() {
		videoAction(videoTarget, videoSource);
	});

	// COUNTER ---------------------------------------- //

	const counterCommonOptions = {
		useEasing: true,
		useGrouping: true,
		separator: '',
		decimal: '.',
	};

	const counterDiscountOptions = {
		...counterCommonOptions,
		suffix: ' руб'
	};

	const counterPagesOptions = {
	  ...counterCommonOptions,
		prefix: '0',
		suffix: '/04'
	};

	// Init

	const counterDiscount = new CountUp('counterDiscount', 0, 0, 0, 3*baseTD/1000, counterDiscountOptions);
	const counterPages = new CountUp('counterPages', 0, 0, 0, 3*baseTD/1000, counterPagesOptions);

	if (!counterDiscount.error) {
		counterDiscount.start();
	} else {
	  console.error(counterDiscount.error);
	}

	if (!counterPages.error) {
		counterDiscount.start();
	} else {
	  console.error(counterPages.error);
	}

	// Start questions button on header click

	startButton.click(function() {
		questionSlider.slick('slickNext');
	});

	// Previous question button click

	prevButtonTarget.click(function() {
		prevButton(questionSlider, finishSlider, foremanSlider);
	});

	// INPUT RADIO OPTIONS EVENTS

	labelRoomType.hover(
		function(e) {
			$('#questionsSliderBG .question-slider__bg__single.' + e.target.htmlFor).removeClass('is-hidden');
		},
		function(e) {
			$('#questionsSliderBG .question-slider__bg__single.' + e.target.htmlFor).addClass('is-hidden');
		}
	);

	inputRoomType.click(function(e) {
		validRadioOptions(e, questionSlider, questionSliderContainer, true);
	});

	planOptions.click(function(e) {
		validRadioOptions(e, finishSlider);
	});

	serviceOptions.click(function(e) {
		validRadioOptions(e, finishSlider, false, false, true);
	});

	// INPUT NUMBER NEXT BUTTON ACTION

	inputNext.click(function(e) {
		inputAction(e, questionSlider);
	});

	// INPUT NUMBER KEYPRESS ENTER EVENT

	$("input[type='number']").keydown(function(e) {
		if (e.keyCode == 9) e.keyCode = 13;

		inputEnter(e, questionSlider, 13);
	});

	// SLICK EVENTS

	questionSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
		// Video animation

		if (nextSlide == 0) {
			videoTarget.removeClass('is-hidden');
		} else {
			videoTarget.addClass('is-hidden');
		}

		// Change direction animation

		if (currentSlide > nextSlide) {
			$(`${questionSliderSingle}:eq(${nextSlide})`).addClass('is-left');
			$(`${questionSliderSingle}:eq(${nextSlide})`).removeClass('is-right');
		} else {
			$(`${questionSliderSingle}:eq(${nextSlide})`).addClass('is-right');
			$(`${questionSliderSingle}:eq(${nextSlide})`).removeClass('is-left');
		}

		// Header contact info animation

		if (nextSlide > 0) {
			contactInfo.addClass('is-hidden');
		} else {
			contactInfo.removeClass('is-hidden');
		}

		// Previous question button, status pages and discount show/hide animation

		if (nextSlide > 0) {
			prevButtonTarget.removeClass('is-hidden');
			pagesStatus.removeClass('is-hidden');
			discountStatus.removeClass('is-hidden');
			contactButton.addClass('is-hidden');
		} else {
			prevButtonTarget.addClass('is-hidden');
			pagesStatus.addClass('is-hidden');
			discountStatus.addClass('is-hidden');
			contactButton.removeClass('is-hidden');
		};

		if (nextSlide === 5) {
			prevButtonTarget.addClass('is-hidden');
			pagesStatus.addClass('is-hidden');
			discountStatus.addClass('is-finish');
			contactButton.removeClass('is-hidden');
			shareButton.removeClass('is-hidden');
			questionSlider.addClass('is-hidden');
			foremanSlider.addClass('is-hidden');
			finishSlider.removeClass('is-hidden');
			questionsSliderBG.addClass('is-finish');
			finishSlider.slick('slickGoTo', 0);
		}

		if (nextSlide === 4) {
			$('#questionsSliderBG .question-slider__bg__single').addClass('is-hidden');
		}
	});

	questionSlider.on('afterChange', function(event, slick, currentSlide){
		// Counter update

		if (currentSlide > 0 && currentSlide < 6) {
			counterDiscount.update((currentSlide - 1) * 1500);
			counterPages.update('0' + currentSlide);
		}

		// Autofocus input on change slide

		if (currentSlide === 2) {
			$("#area").focus();
		} else
		if (currentSlide === 3) {
			$("#room").focus();
		} else
		if (currentSlide === 4) {
			$("#height").focus();
		}
	});

	// COUNTER UPDATE

	finishSlider.on('afterChange', function(event, slick, currentSlide){
		if (currentSlide < 3) {
			counterDiscount.update(currentSlide * 1500 + 4 * 1500);
		}
	});

	// SHARE BUTTON

	$('.share').ShareLink({
		title: 'Name company',
		text: 'Name company - description',
		image: 'http://artgrovestudio.com/quezt/img/header-bg.jpg',
		url: 'http://artgrovestudio.com/quezt/',
		class_prefix: 'share_',
	});

	// AJAX CONTACT FORM SENDING

	sendButton.click(function(e) {
		if ( $('#name').hasClass('valid') && $('#tel').hasClass('valid') && $('#email').hasClass('valid') ) {
			if (!$(e.target).hasClass('estimate')) {
				if (!$(e.target).hasClass('success')) {
					ajaxRequest(targetForm, $('#sendButton'));
				}
			} else {
				finishSlider.slick('slickNext');
			}
		} else {
			alert('Заполните все поля!');
		}
	});

	buyButton.click(function(e) {
		ajaxRequest(targetForm, $('#buyButton'));
	});

	$.validate({
		lang: 'ru',
		borderColorOnError: '#ef5350'
	});

	// FINISH LOADING ACTION

	setTimeout(function () {
		$('body').removeClass('loading');
	}, 2500);

}());