export function videoAction(target, source) {
	if (target.hasClass('is-open')) {
		target.removeClass('is-open');
		source.get(0).pause();
	} else {
		source.lazy();
		target.addClass('is-open');
		source.get(0).play();
	}
}