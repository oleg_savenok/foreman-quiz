export function inputEnter(event, sliderAction, keyCode) {
	if (event.keyCode === keyCode) {
		$(event.target).validate();

		if ($(event.target).hasClass('valid')) {
			sliderAction.slick('slickNext');
		}
	}
}